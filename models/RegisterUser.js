var mongoose = require('mongoose');
var mongooseUniqueValidator = require('mongoose-unique-validator');
console.log('entered register');
let RegisterUser = mongoose.Schema({  
    
    fname : String,
    lname : String,
    mobilenumber : {type : String,required : true,unique : true},
    email : {type : String,required : true, unique : true},
    bloodG :  {type : String, required : true},
    age :  {type : String, required : true},
    gender :  {type : String, required : true},
    place :  {type : String, required : true},
    pass : {type  : String, required : true},
    posts : [{
            Req_bloodG :{type : String},
            place : {type : String},
            LatLong : {type : String},
    }]  
}) ;

RegisterUser.plugin(mongooseUniqueValidator);

// //making Email and Mobile Number unique
// RegisterUser.index({
//     MobileNo :1,
//     email :1
// },{unique : true,});

let reg = module.exports = mongoose.model('reg', RegisterUser);
