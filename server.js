var express= require('express');
var router = express.Router();
var mongoose = require('mongoose');
let reg = require('./models/RegisterUser');
var bodyParser = require('body-parser'); 
var p = require('./models/posts');
mongoose.connect("mongodb://localhost:27017/bloodbank", { useNewUrlParser: true });
 var db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
 db.once("open", function(callback) {
     console.log("Connection succeeded.");
  });
 const app = express();
 app.use(bodyParser.urlencoded({
     extended: true
 }));
 app.use(bodyParser.json());

 // Bring in Models

 let regis = new reg();

app.post('/insertUserData',function(req , res){
    regis.fname = req.body.fname;
    regis.lname = req.body.lname;
    regis.mobilenumber = req.body.mobilenumber;
    regis.email = req.body.email;
    regis.pass = req.body.pass;
    regis.bloodG = req.body.bloodG;
    regis.age = req.body.age;
    regis.gender = req.body.gender;
    regis.place = req.body.place;
    regis.posts = req.body.posts;
    console.log(regis._id)
    
    regis.save(function(err){
        
        if(err){
            res.send("ERROR :" + err);    
        }
        else{
            res.send("1"); 
        }
      
    });
   

});


var ps = new p();
// inserting posts
app.post('/posts/:id',function(req , res){

//    reg.findById(req.params.id,function(err, user){
    let item = {
        Req_bloodG :req.body.Req_bloodG,
         place : req.body.place,
         LatLong : req.body.LatLong,
     }
     console.log(item)
     reg.findOneAndUpdate({_id :  req.params.id}, {$push : {posts : item}},function(err,sucess){
   
             if(err){
                res.send("ERROR :" + err);    
            }
                res.send(sucess); 
             
     } );

});
app.get('/getposts/:id',function(req,res){
    p.find({postedBy : req.params.id}).populate('postedBy', 'Fname Lname MobileNo place').exec(function(err,post){
     res.send(post);
    });
});

app.get('/getalldata',function(req,res){
    reg.find({},'fname lname mobilenumber place posts',function(err,docs){
        if (err){
             res.send('error occured in the database'+ err);
    }
        
    else{
                res.send(docs)
            }
       
    });     
 
   
});
// Login TO DB==================================================================
app.post('/login',function(req,res){
    reg.findOne({email : req.body.email} , function(err,user){
        console.log(user)
        if (err) throw err;
        if(user == null){
        return res.status(404).end('Wrong Email........');
        }
        else if(req.body.pass === user.pass){
            res.send(user);
        }
        else{
            return res.status(400).end('PASSWORD IS WRONG');
        }          
    });  
});
    

app.listen(3000, function(){
    console.log('Server started on port 3000...');
  });